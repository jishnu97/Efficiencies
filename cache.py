import time

import numpy
def matrixGenerator(rows, cols):
        return numpy.random.randint(rows*cols, size=(rows,cols))

def matrixAdder(mat, fast):
    sum = 0
    if fast:
        for i in range(0,len(mat)):
            for j in range(0, len(mat[0])):
                    sum+=mat[i][j]
    else:
        for j in range(0,len(mat[0])):
            for i in range(0,len(mat)):
                sum+=mat[i][j]
    return sum 

mat = matrixGenerator(10000,10000)
print "Matrix:"
print mat
print "Sum is",
start = time.clock()
print matrixAdder(mat, True)
print "Time taken when elements are added by column:", time.clock() - start
start = time.clock()
print matrixAdder(mat, False) #This takes a full 14 seconds longer on my Mac
print "Time taken when elements are added by row:", time.clock() - start


